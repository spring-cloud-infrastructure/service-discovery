# Service Discovery

Check out [documentation](https://cloud.spring.io/spring-cloud-netflix/reference/html/) for more info about Netflix's
Eureka Service Discovery.

## 1. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

## 2. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
